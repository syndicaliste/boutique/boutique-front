import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {Products} from "../../models/products";
import {ProductsService} from "../../services/products.service";

@Component({
  selector: 'app-products-single',
  templateUrl: './products-single.component.html',
  styleUrls: ['./products-single.component.css']
})
export class ProductsSingleComponent implements OnInit {
  productId: string | undefined | null;
  product$!: Observable<Products>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ProductsService
  ) { }

  ngOnInit(): void {
    this.productId = this.route.snapshot.paramMap.get('id');
    this.product$ = this.service.getProduct(this.productId);
  }

}
