import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Products} from "../../models/products";
import {ProductsService} from "../../services/products.service";

@Component({
  selector: 'app-products-all',
  templateUrl: './products-all.component.html',
  styleUrls: ['./products-all.component.css']
})
export class ProductsAllComponent implements OnInit {

  products$!: Observable<Products[]>;

  constructor(private service: ProductsService) { }

  ngOnInit(): void {
    this.products$ = this.service.getProducts();
  }

}
