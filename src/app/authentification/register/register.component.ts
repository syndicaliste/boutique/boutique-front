import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {MustMatch} from "../MustMatch";
import {AuthentificationService} from "../../services/authentification.service";
import {Users} from "../../models/users";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  // @ts-ignore
  registerForm: FormGroup;
  // @ts-ignore
  newUser: Users;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private service: AuthentificationService, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      confirmPassword: ['', Validators.required],
    }, {
      validators: [
        MustMatch('password', 'confirmPassword'),
        MustMatch('email', 'confirmEmail')
      ]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      console.log("erreur register invalid");
      return;
    }
    else {
      this.registerForm.value.confirmEmail = true;
      var data = {
        "username": this.registerForm.value.username,
        "email": this.registerForm.value.email,
        "emailVerified": this.registerForm.value.confirmEmail,
        "password": this.registerForm.value.password
      }
      this.service.postUser(data).subscribe();

      alert("Inscription reussie !");
      this.router.navigate(['/']);
    }
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
