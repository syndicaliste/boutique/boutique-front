import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Products} from "../models/products";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map, retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private readonly apiUrl = environment.apiBaseUrl;
  private productsUrl = this.apiUrl + 'products/';

  constructor(private readonly client: HttpClient) { }

  displayError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    errorMessage += '\n\nMerci de réessayer ultérieurement.';
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getProducts(): Observable<Products[]> {
    return this.client.get<any>(this.productsUrl)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => this.displayError(error))
      )
  }

  getProduct(id: string | null): Observable<Products> {
    return this.client.get<any>(this.productsUrl + '/' + id)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => this.displayError(error))
      )
  }
}
