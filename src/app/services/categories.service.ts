import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Categories} from "../models/categories";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map, retry} from "rxjs/operators";
import {Products} from "../models/products";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  private readonly apiUrl = environment.apiBaseUrl;
  private categoriesUrl = this.apiUrl + 'categories/';

  constructor(private readonly client: HttpClient) { }

  displayError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    errorMessage += '\n\nMerci de réessayer ultérieurement.';
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getCategories(): Observable<Categories[]> {
    return this.client.get<any>(this.categoriesUrl)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => this.displayError(error))
      )
  }

  getCategorie(id: string | null): Observable<Categories> {
    return this.client.get<any>(this.categoriesUrl + '/' + id)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => this.displayError(error))
      )
  }
}
