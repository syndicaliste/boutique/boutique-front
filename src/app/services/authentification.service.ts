import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class AuthentificationService {
  // @ts-ignore
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  private readonly apiUrl = environment.apiBaseUrl;
  private signupUrl = this.apiUrl + 'signup/';

  constructor(private http: HttpClient) { }

  postUser(data : any) : Observable<any[]> {
    return this.http.post<any>(this.signupUrl, JSON.stringify(data), this.httpOptions)
      .pipe(
        // @ts-ignore
        catchError(this.handleError('post', data))
      )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
