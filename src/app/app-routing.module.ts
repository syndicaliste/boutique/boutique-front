import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductsAllComponent} from "./products/products-all/products-all.component";
import {ProductsSingleComponent} from "./products/products-single/products-single.component";
import {RegisterComponent} from "./authentification/register/register.component";

const routes: Routes = [
  { path: '', component: ProductsAllComponent },
  { path: 'produits/:id', component: ProductsSingleComponent},
  { path: 'signup', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
