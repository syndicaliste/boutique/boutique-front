export class Products {

  constructor(
    public id: number,
    public name: string,
    public description: string,
    public categoriesId: number,
    public reference: string,
    public price: number,
    public size: string,
    public image: string
    ) { }

}
