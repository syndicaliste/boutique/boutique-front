export class Users {

  id?: string;
  realm?: string;
  username!: string;
  email!: string;
  emailVerified!: number;
  verificationToken?: string;
  password!: string;

  constructor (values: Object = {}) {
    Object.assign(<Users> this, values);
  }

}
